<?php require '../login/valida_sessao.inc';?>
<?php require '../padrao/cabecalho.php';?>
<?php require '../padrao/menu.php';?>

<?php

// conectar com BD
include 'conectabd.inc.php';

// consulta sql 
$query = "select * from produto order by nome";

// executar sql
$executar = mysqli_query($link, $query);

// pecorrer a tabela do BD
echo "<table class='table table-dark table-hover'>"; 
echo "<tr><th>Produtos Listados</th><th>Código de Barras</th><th>Nome</th><th>Categoria</th><td>Tamanho</td><th>Marca</th><th>Descrição</th><th>Preço</th>
     <td>Quantidade em Estoque</td><th>Opções</th></tr>";
while ($linha = mysqli_fetch_array($executar)){
$id_produto = $linha['id_produto'];
$codigo =$linha['cod'];
$nome = $linha['nome'];
$categoria = $linha['categoria'];
$tamanho = $linha['tamanho'];
$marca = $linha['marca'];
$descricao = $linha['decricao'];
$valor = $linha['valor'];
$qt_estoque = $linha['qt_estoque'];
$id_usuario = $linha['id_usuario'];
$id_fornecedor = $linha['id_fornecedor'];

    echo "<tr><td>$id_produto</td><td>$codigo</td><td>$nome</td><td>$categoria</td><td>$tamanho</td><td>$marca</td><td>$descricao</td><td>$valor</td><td>$qt_estoque</td><td>
            <a href='index.php?id_produto=$id_produto&cod=$codigo&nome=$nome&categoria=$categoria&tamanho=$tamanho&marca=$marca&decricao=$descricao&valor=$valor&qt_estoque=$qt_estoque' class='btn btn-primary btn-sm' style='width:100px';>Editar</a> | 
            <a href='excluirproduto.php?id_produto=$id_produto' class='btn btn-primary btn-sm' style='width:100px';>Excluir</a>	
            </td></tr>";
}
echo "</table>";
// fechar a conexão
mysqli_close($link);

require '../padrao/rodape.php';