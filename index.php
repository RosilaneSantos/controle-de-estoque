<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Login</title>
        <!-- Bootstrap -->
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
      <link rel="stylesheet" href="css/style.css">
  </head>
    <body>

        <div class="container" class="col-md-6 offset-md-3">

          <!-- Alerta-->
          <?php require './padrao/alertas.php';?>

          <h5 class="card-title text-center">Login</h5>
          <br>
          <form action="login/login.php" method="post">
            <div class="form-group">
              <input type="text" name="email" class="form-control" placeholder="Email">
            </div>
            <div class="form-group">
              <input type="password" class="form-control" name="senha" placeholder="Senha">
            </div>
            <div class="form-check">
              <input type="checkbox" class="form-check-input">
              <label class="form-check-label" for="exampleCheck1">Lembrar Usuário</label>
            </div>
            <br>
            <button type="submit" class="btn btn-info btn-lg btn-block" name="entrar">
              Login
            </button>
            <br>
            <a href="./usuario/cadastro.php" align="center">Cadastrar-se</a>
          </form>
         </div>


        <script src="js/bootstrap.min.js" type="text/javascript"></script>
    </body>
</html>
