<?php
// inicia ou recupera dados de sessão existente
session_start();
// obtém os valores digitados
$senha_atual = $_POST["senhaAtual"];
$novaSenha = $_POST["senhaNova"];
$vNovaSenha = $_POST["csenhaNova"];
$crypSenha = hash('sha256', $senha_atual);

$user = $_SESSION["emailUser"];
$senha_banco = $_SESSION['senhaUser'];
// verifica se a senha atual informada confere com
// a senha informada no login
if ($crypSenha == $senha_banco)
{
    // verifica se a nova senha digitada confere
    // com a senha digitada na segunda vez
    if ($novaSenha == $vNovaSenha) {
        $crypNovaSenha = hash('sha256', $novaSenha);
        // acesso ao banco de dados
        include_once "../padrao/conectabd.inc.php";

        $query = "UPDATE usuario
        SET
        senha = '$crypNovaSenha'
        WHERE email = '$user';";

        $resultado = mysqli_query($link, $query);

        // libera a área de memória onde está o resultado
      	mysqli_free_result($result);

        mysqli_close($link);
        // atualiza a variavel de sessão com nova senha
        $_SESSION['senhaUser'] = $crypNovaSenha;
        //echo '<p align="center">Senha atualizada com sucesso.</p><br>';
        sleep(1);
        header("Location: ../usuario/index.php?u_altSN");
    } else {
        //echo '<p align="center">Novas senhas digitadas não conferem.</p>';
        sleep(1);
        header("Location: ../usuario/alterarSenha.php?u_altSNC");
    }
} else {
    //echo '<p align="center">Senha atual não confere.</p>';
    sleep(1);
    header("Location: ../usuario/alterarSenha.php?u_altAnC");
}

?>
