<?php require '../login/valida_sessao.inc';?>
<?php require '../padrao/cabecalho.php';?>
<?php require '../padrao/menu.php';?>
<?php

// conectar com BD
include 'conectabd.inc.php';

// consulta sql 
$query = "select * from usuario order by nome";

// executar sql
$executar = mysqli_query($link, $query);

// pecorrer a tabela do BD
echo "<table class='table table-dark table-hover'>"; 
echo "<tr><th>Funcionários Listados</th><th>Razão  Social</th><th>CNPJ</th><th>E-mail</th><th>Endereço</th><th>Telefone</th><th>Responsável</th><th>Nome</th><th>CPF</th><th>Sexo</th><th>Tipo</th><th>Opções</th></tr>";
while ($linha = mysqli_fetch_array($executar)){
	$idsu = $linha['id_usuario'];
	$rz = $linha['razao_social'];
	$cnpj = $linha['cnpj'];
	$email = $linha['email'];
	$endereco = $linha['endereco'];
	$tel = $linha['telefone'];
	$res = $linha['responsavel'];
    $nome = $linha['nome'];
    $cpf = $linha['cpf'];
    $sexo = $linha['sexo'];
	$tipo = $linha['tipo'];
    echo "<tr><td>$idsu</td><td>$rz</td><td>$cnpj</td><td>$email</td><td>$endereco</td><td>$tel</td><td>$res</td><td>$nome</td><td>$cpf</td><td>$sexo</td><td>$tipo</td><td>
            <a href='index.php?id_usuario=$idsu&razao_social=$rz&cnpj=$cnpj&email=$email&endereco=$endereco&telefone=$tel&responsavel=$res&nome=$nome&cpf=$cpf&sexo=$sexo&tipo=$tipo' class='btn btn-primary btn-sm' style='width:100px';>Editar</a> | 
            <a href='exclusao.php?id_usuario=$idsu' class='btn btn-primary btn-sm' style='width:100px';>Excluir</a>	
            </td></tr>";
}
echo "</table>";
// fechar a conexão
mysqli_close($link);

require '../padrao/rodape.php';