<?php require '../login/valida_sessao.inc';?>
<?php require '../padrao/cabecalho.php';?>
<?php require '../padrao/menu.php';?>

<?php
	//Pega os dados do user logado
	$user = $_SESSION["emailUser"];
	require "../padrao/conectabd.inc.php";
	$resultado = mysqli_query($link, "SELECT * FROM usuario where email='$user'");
	$dados = mysqli_fetch_array($resultado);
	$rSocial = $dados["razao_social"];
  $cnpj = $dados["cnpj"];
?>

    <div class="container" class="col-md-6 offset-md-3">

			<!-- Alerta-->
			<?php require '../padrao/alertas.php';?>

			<h5 class="card-title text-center">Cadastro</h5>
      <br>
      <form action="addUser.php" method="post">
        <input type="hidden" name="tipo" value="funcionario">
        <input type="hidden" name="rSocial" value="<?php echo $rSocial; ?>">
        <input type="hidden" name="cnpj" value="<?php echo $cnpj; ?>">
        <div class="form-row">
          <div class="col">
            <input name="nome" type="text" class="form-control" placeholder="Nome" required="">
          </div>
          <div class="col">
            <input name="cpf" type="number" class="form-control" placeholder="CPF" required="">
          </div>
        </div>
        <br>
        <div class="form-group">
					<input name="email" type="email" class="form-control" placeholder="@mail.com" required="">
				</div>
        <div class="form-group">
          <input name="endereco" type="text" class="form-control" placeholder="Endereço" required="">
        </div>
        <div class="form-group">
          <input name="tel" type="text" class="form-control" placeholder="Telefone">
        </div>
        <div class="form-group">
          <select class="form-control" name="sexo" required>
            <option value="">Sexo</option>
            <option value="M">Masculino</option>
            <option value="F">Feminino</option>
          </select>
        </div>
        <div class="form-row">
          <div class="col">
            <input name="senha" type="password" class="form-control" placeholder="Senha" required="">
          </div>
          <div class="col">
            <input name="cSenha" type="password" class="form-control" placeholder="Confirmar senha" required="">
          </div>
        </div>
        <br>
        <button type="submit" class="btn btn-info btn-lg btn-block" name="logFun">
          Cadastrar
        </button>
      </form>
     </div>


<?php require '../padrao/rodape.php';?>
