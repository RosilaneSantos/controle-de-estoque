<?php
  // Se o usuário clicou no botão cadastrar efetua as ações
  if (isset($_POST['empresa'])) {

    // Recupera os dados dos campos
    $tipo = $_POST["tipo"];
    $rSocial = $_POST["rSocial"];
  	$cnpj = $_POST["cnpj"];
  	$email = $_POST["email"];
    $endereco = $_POST["endereco"];
    $tel = $_POST["tel"];
    $responsavel = $_POST["responsavel"];
  	$senha = $_POST["senha"];
  	$crypSenha = hash('sha256', $senha); // criptografando a senha
  	$vSenha = $_POST["cSenha"];

    if ($senha == $vSenha) { // ira validar se a senha é igual a confirmação de senha se sim (incluimos os dados) se não retornamos uma mensagem avisando que ERRO na confirmação da senha.

      include_once "../padrao/conectabd.inc.php";

      //Usuario
      $query = "INSERT INTO usuario
    			    (razao_social, cnpj, email, senha, endereco, telefone, responsavel, tipo)
    				values ('$rSocial', '$cnpj', '$email', '$crypSenha', '$endereco', '$tel', '$responsavel', '$tipo');";
      // $result = $link->query($query)
      if ($result = mysqli_query($link, $query)) {
          //echo '<p align="center">Cadastrado com SUCESSO!</p><br>';
          sleep(1);
          header("Location: ../index.php?u_cadastro");
        } else {
          //echo '<p align="center">ERRO! no cadastro</p><br>';
          sleep(1);
          header("Location: ../index.php?u_cadastro");
        }
        die();

        // libera a área de memória onde está o resultado
      	mysqli_free_result($result);

        // fecha a conexão
        mysqli_close($link);
    } else {
        //echo '<p align="center">ERRO! As senhas prescisão ser iguais</p><br>';
        sleep(1);
        header("Location: ../usuario/cadastro.php?u_erroS");
    	}
    	die();
  }


  // Se o usuário clicou no botão cadastrar efetua as ações
  if (isset($_POST['funcionario'])) {

    // Recupera os dados dos campos
    $tipo = $_POST["tipo"];
    $nome = $_POST["nome"];
    $cpf = $_POST["cpf"];
  	$email = $_POST["email"];
    $endereco = $_POST["endereco"];
    $tel = $_POST["tel"];
    $rSocial = $_POST["rSocial"];
  	$cnpj = $_POST["cnpj"];
    $sexo = $_POST["sexo"];
  	$senha = $_POST["senha"];
  	$crypSenha = hash('sha256', $senha); // criptografando a senha
  	$vSenha = $_POST["cSenha"];

    if ($senha == $vSenha) { // ira validar se a senha é igual a confirmação de senha se sim (incluimos os dados) se não retornamos uma mensagem avisando que ERRO na confirmação da senha.

      include_once "../padrao/conectabd.inc.php";

      //Usuario
      $query = "INSERT INTO usuario
    			    (razao_social, cnpj, email, senha, endereco, telefone, nome, cpf, sexo, tipo)
    				values ('$rSocial', '$cnpj', '$email', '$crypSenha', '$endereco', '$tel', '$nome', '$cpf', '$sexo', '$tipo');";
      // $result = $link->query($query)
      if ($result = mysqli_query($link, $query)) {
          //echo '<p align="center">Cadastrado com SUCESSO!</p><br>';
          sleep(1);
          header("Location: ../index.php?u_cadastro");
        } else {
          //echo '<p align="center">ERRO! no cadastro</p><br>';
          sleep(1);
          header("Location: ../index.php?u_cadastro");
        }
        die();

        // libera a área de memória onde está o resultado
      	mysqli_free_result($result);

        // fecha a conexão
        mysqli_close($link);
    } else {
        //echo '<p align="center">ERRO! As senhas prescisão ser iguais</p><br>';
        sleep(1);
        header("Location: ../usuario/cadastroFun.php?u_erroS");
    	}
    	die();
  }


  // Se o usuário clicou no botão cadastrar efetua as ações
  if (isset($_POST['logFun'])) {

    // Recupera os dados dos campos
    $tipo = $_POST["tipo"];
    $nome = $_POST["nome"];
    $cpf = $_POST["cpf"];
    $email = $_POST["email"];
    $endereco = $_POST["endereco"];
    $tel = $_POST["tel"];
    $rSocial = $_POST["rSocial"];
    $cnpj = $_POST["cnpj"];
    $sexo = $_POST["sexo"];
    $senha = $_POST["senha"];
    $crypSenha = hash('sha256', $senha); // criptografando a senha
    $vSenha = $_POST["cSenha"];

    if ($senha == $vSenha) { // ira validar se a senha é igual a confirmação de senha se sim (incluimos os dados) se não retornamos uma mensagem avisando que ERRO na confirmação da senha.

      include_once "../padrao/conectabd.inc.php";

      //Usuario
      $query = "INSERT INTO usuario
              (razao_social, cnpj, email, senha, endereco, telefone, nome, cpf, sexo, tipo)
            values ('$rSocial', '$cnpj', '$email', '$crypSenha', '$endereco', '$tel', '$nome', '$cpf', '$sexo', '$tipo');";
      // $result = $link->query($query)
      if ($result = mysqli_query($link, $query)) {
          //echo '<p align="center">Cadastrado com SUCESSO!</p><br>';
          sleep(1);
          header("Location: ../usuario/cadastroLogFun.php?u_cadastroF");
        } else {
          //echo '<p align="center">ERRO! no cadastro</p><br>';
          sleep(1);
          header("Location: ../usuario/cadastroLogFun.php?u_cadastroF");
        }
        die();

        // libera a área de memória onde está o resultado
      	mysqli_free_result($result);

        // fecha a conexão
        mysqli_close($link);
    } else {
        //echo '<p align="center">ERRO! As senhas prescisão ser iguais</p><br>';
        sleep(1);
        header("Location: ../usuario/cadastroLogFun.php?u_erroS");
    	}
    	die();
  }
