<?php require '../padrao/cabecalho.php';?>
<?php require '../padrao/menu.php';?>


    <div class="container" class="col-md-6 offset-md-3">

      <!-- Alerta-->
      <?php require '../padrao/alertas.php';?>

      <h5 class="card-title text-center">Cadastro Empresa</h5>
      <br>
      <form action="addUser.php" method="post">
        <input type="hidden" name="tipo" value="empresa">
        <div class="form-row">
          <div class="col">
            <input name="rSocial" type="text" class="form-control" placeholder="Razão social" required="">
          </div>
          <div class="col">
            <input name="cnpj" type="number" class="form-control" placeholder="CNPJ" required="">
          </div>
        </div>
        <br>
        <div class="form-group">
					<input name="email" type="email" class="form-control" placeholder="@mail.com" required="">
				</div>
        <div class="form-group">
          <input name="endereco" type="text" class="form-control" placeholder="Endereço" required="">
        </div>
        <div class="form-group">
          <input name="tel" type="text" class="form-control" placeholder="Telefone">
        </div>
        <div class="form-group">
          <input name="responsavel" type="text" class="form-control" placeholder="Responsável da empresa" required="">
        </div>
        <div class="form-row">
          <div class="col">
            <input name="senha" type="password" class="form-control" placeholder="Senha" required="">
          </div>
          <div class="col">
            <input name="cSenha" type="password" class="form-control" placeholder="Confirmar senha" required="">
          </div>
        </div>
        <br>
        <button type="submit" class="btn btn-info btn-lg btn-block" name="empresa">
          Cadastrar
        </button>
        <br>
        <a href="cadastroFun.php" align="center">Cadastro funcionário</a>
      </form>
     </div>


<?php require '../padrao/rodape.php';?>

	<footer align="center">
 		<br>
 		<br>
     	<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Copyright © 2019 Controle de Estoque - Todos os direitos reservados</p>
	</footer> 
