<?php require '../login/valida_sessao.inc';?>
<?php require '../padrao/cabecalho.php';?>
<?php require '../padrao/menu.php';?>

	<!-- Alerta-->
	<?php require '../padrao/alertas.php';?>

<?php
	//Pega os dados do user logado
	$user = $_SESSION["emailUser"];
	require "../padrao/conectabd.inc.php";
	$resultado = mysqli_query($link, "SELECT * FROM usuario where email='$user'");
	$dados = mysqli_fetch_array($resultado);
	$id = $dados["id_usuario"];
	$tipo = $dados["tipo"];
	// codição
	if ($tipo == 'empresa') {
		$rSocial = $dados["razao_social"];
  	$cnpj = $dados["cnpj"];
  	$email = $dados["email"];
    $endereco = $dados["endereco"];
    $tel = $dados["telefone"];
    $responsavel = $dados["responsavel"];

?>
	<!-- mostra os dados -->
	<div align="center">
		<br><br>
		<p><?php echo $rSocial; ?></p>
		<p><?php echo $cnpj; ?></p>
		<p><?php echo $email; ?></p>
		<p><?php echo $endereco; ?></p>
		<p><?php echo $tel; ?></p>
		<p><?php echo $responsavel; ?></p>
		<br>
		<a href="alterar.php">alterar dados</a>
		<br>
		<a href="alterarSenha.php">alterar senha</a>
		<br>
		<a href="exclusao.php">excluir conta</a>
	</div>

 <?php
} elseif ($tipo == 'funcionario') {
	$nome = $dados["nome"];
	$cpf = $dados["cpf"];
	$email = $dados["email"];
	$endereco = $dados["endereco"];
	$tel = $dados["telefone"];
	$rSocial = $dados["razao_social"];
	$cnpj = $dados["cnpj"];
	$sexo = $dados["sexo"];
  ?>

	<!-- mostra os dados -->
	<br><br>
	<div align="center">
		<p><?php echo $nome; ?></p>
		<p><?php echo $cpf; ?></p>
		<p><?php echo $email; ?></p>
		<p><?php echo $endereco; ?></p>
		<p><?php echo $tel; ?></p>
		<p><?php echo $rSocial; ?></p>
		<p><?php echo $cnpj; ?></p>
		<p>
			<?php
				if($sexo == 'M'){
					echo 'Masculino';
				}elseif ($sexo == 'F'){
					echo 'Feminino';
				}
			?>
		</p>
		<br>
		<a href="alterar.php">alterar dados</a>
		<br>
		<a href="alterarSenha.php">alterar senha</a>
		<br>
		<a href="exclusao.php">excluir conta</a>
	</div>

	<?php
	}
	 ?>


<?php require '../padrao/rodape.php';?>
