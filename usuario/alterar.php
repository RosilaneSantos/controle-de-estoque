<?php require '../login/valida_sessao.inc';?>
<?php require '../padrao/cabecalho.php';?>

<?php
	//Pega os dados do user logado
	$user = $_SESSION["emailUser"];
	require "../padrao/conectabd.inc.php";
	$resultado = mysqli_query($link, "SELECT * FROM usuario where email='$user'");
	$dados = mysqli_fetch_array($resultado);
	$tipo = $dados["tipo"];
	$id = $dados["id_usuario"];
	// codição
	if ($tipo == 'empresa') {
		$rSocial = $dados["razao_social"];
  	$cnpj = $dados["cnpj"];
    $endereco = $dados["endereco"];
    $tel = $dados["telefone"];
    $responsavel = $dados["responsavel"];
?>
	<!-- mostra os dados -->
	<div class="container" class="col-md-6 offset-md-3">
		<h5 class="card-title text-center">Alterar dados</h5>
		<br>
		<form action="alteracao.php" method="post">
			<input type="hidden" name="id" value="<?php echo $id; ?>">
			<div class="form-row">
				<div class="col">
					<input name="rSocial" type="text" class="form-control" value="<?php echo $rSocial; ?>" required="">
				</div>
				<div class="col">
					<input name="cnpj" type="number" class="form-control" value="<?php echo $cnpj; ?>" required="">
				</div>
			</div>
			<br>
			<div class="form-group">
				<input name="endereco" type="text" class="form-control" value="<?php echo $endereco; ?>" required="">
			</div>
			<div class="form-group">
				<input name="tel" type="text" class="form-control" value="<?php echo $tel; ?>">
			</div>
			<div class="form-group">
				<input name="responsavel" type="text" class="form-control" value="<?php echo $responsavel; ?>" required="">
			</div>
			<br>
			<button type="submit" class="btn btn-info btn-lg btn-block" name="empresa">
				Salvar
			</button>
		</form>
	 </div>

 <?php
} elseif ($tipo == 'funcionario') {
	$nome = $dados["nome"];
	$cpf = $dados["cpf"];
	$endereco = $dados["endereco"];
	$tel = $dados["telefone"];
	$rSocial = $dados["razao_social"];
	$cnpj = $dados["cnpj"];
	$sexo = $dados["sexo"];
  ?>

		<!-- mostra os dados -->
		<div class="container" class="col-md-6 offset-md-3">
      <h5 class="card-title text-center">Alterar dados</h5>
      <br>
      <form action="alteracao.php" method="post">
        <input type="hidden" name="id" value="<?php echo $id; ?>">
        <div class="form-row">
          <div class="col">
            <input name="nome" type="text" class="form-control" value="<?php echo $nome; ?>" required="">
          </div>
          <div class="col">
            <input name="cpf" type="number" class="form-control" value="<?php echo $cpf; ?>" required="">
          </div>
        </div>
        <br>
        <div class="form-group">
          <input name="endereco" type="text" class="form-control" value="<?php echo $endereco; ?>" required="">
        </div>
        <div class="form-group">
          <input name="tel" type="text" class="form-control" value="<?php echo $tel; ?>">
        </div>
        <div class="form-row">
          <div class="col">
            <input name="rSocial" type="text" class="form-control" value="<?php echo $rSocial; ?>" required="">
          </div>
          <div class="col">
            <input name="cnpj" type="number" class="form-control" value="<?php echo $cnpj; ?>" required="">
          </div>
        </div>
        <br>
        <div class="form-group">
          <select class="form-control" name="sexo" value="<?php echo $sexo; ?>" required>
            <option value="">Sexo</option>
            <option value="M">Masculino</option>
            <option value="F">Feminino</option>
          </select>
        </div>
        <br>
        <button type="submit" class="btn btn-info btn-lg btn-block" name="funcionario">
          Salvar
        </button>
      </form>
     </div>

		 <?php
		 }
			?>


<?php require '../padrao/rodape.php';?>
