<?php require '../padrao/cabecalho.php';?>
<?php require '../padrao/menu.php';?>

    <div class="container" class="col-md-6 offset-md-3">

      <!-- Alerta-->
      <?php require '../padrao/alertas.php';?>

      <h5 class="card-title text-center">Cadastro Funcionário</h5>
      <br>
      <form action="addUser.php" method="post">
        <input type="hidden" name="tipo" value="funcionario">
        <div class="form-row">
          <div class="col">
            <input name="nome" type="text" class="form-control" placeholder="Nome" required="">
          </div>
          <div class="col">
            <input name="cpf" type="number" class="form-control" placeholder="CPF" required="">
          </div>
        </div>
        <br>
        <div class="form-group">
					<input name="email" type="email" class="form-control" placeholder="@mail.com" required="">
				</div>
        <div class="form-group">
          <input name="endereco" type="text" class="form-control" placeholder="Endereço" required="">
        </div>
        <div class="form-group">
          <input name="tel" type="text" class="form-control" placeholder="Telefone">
        </div>
        <div class="form-row">
          <div class="col">
            <input name="rSocial" type="text" class="form-control" placeholder="Razão social" required="">
          </div>
          <div class="col">
            <input name="cnpj" type="number" class="form-control" placeholder="CNPJ" required="">
          </div>
        </div>
        <br>
        <div class="form-group">
          <select class="form-control" name="sexo" required>
            <option value="">Sexo</option>
            <option value="M">Masculino</option>
            <option value="F">Feminino</option>
          </select>
        </div>
        <div class="form-row">
          <div class="col">
            <input name="senha" type="password" class="form-control" placeholder="Senha" required="">
          </div>
          <div class="col">
            <input name="cSenha" type="password" class="form-control" placeholder="Confirmar senha" required="">
          </div>
        </div>
        <br>
        <button type="submit" class="btn btn-primary btn-lg" style="width:182px;" name="funcionario">
          Cadastrar
        </button>
		<a href="http://localhost/telas_pi/usuario/" type="submit" class="btn btn-primary btn-lg" style="width:182px;">Cancelar</a>
        <br>
        <a href="cadastro.php" align="center">Cadastro empresa</a>
      </form>
     </div>


<?php require '../padrao/rodape.php';?>
   <footer align="center">
 		<br>
 		<br>
     	<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Copyright © 2019 Controle de Estoque - Todos os direitos reservados</p>
	</footer> 