<?php require '../login/valida_sessao.inc';?>
<?php require '../padrao/cabecalho.php';?>

  <div class="container" class="col-md-6 offset-md-3">

    <!-- Alerta-->
    <?php require '../padrao/alertas.php';?>

    <h5 class="card-title text-center">Alterar senha</h5>
			<form method="POST" action="alteracaoSenha.php">
        <div class="form-group">
					<input name="senhaAtual" type="password" class="form-control" placeholder="Senha Atual" required="">
				</div>
				<div class="form-row">
					<div class="col">
						<input name="senhaNova" type="password" class="form-control" placeholder="Nova Senha" required="">
					</div>
					<div class="col">
			    	<input name="csenhaNova" type="password" class="form-control" placeholder="Confirme a nova senha" required="">
			    </div>
				</div>
		    <br>
        <button type="submit" class="btn btn-info btn-lg btn-block" name="empresa">
          Salvar
        </button>
	   </form>
	</div>


<?php require '../padrao/rodape.php';?>
