<?php require '../login/valida_sessao.inc';?>
<?php require '../padrao/cabecalho.php';?>
<?php require '../padrao/menu.php';?>

<?php

// conectar com BD
include 'conectabd.inc.php';

// consulta sql 
$query = "select * from fornecedor order by nome";

// executar sql
$executar = mysqli_query($link, $query);

// pecorrer a tabela do BD
echo "<table class='table table-dark table-hover'>"; 
echo "<tr><th>Fornecedores Listados</th><th>Nome</th><th>Distribuidora</th><th>E-mail</th><th>Contato</th><th>ID Usuário</th><th>Opções</th></tr>";
while ($linha = mysqli_fetch_array($executar)){
    $id= $linha['id_fornecedor'];
    $nome= $linha['nome'];
    $distribuidora = $linha['distribuidora'];
    $email = $linha['email'];
	$telefone = $linha['telefone'];
	$id_usuario = $linha['id_usuario'];
    echo "<tr><td>$id</td><td>$nome</td><td>$distribuidora</td><td>$email</td><td>$telefone</td><td>$id_usuario</td><td>
            <a href='index.php?id_fornecedor=$id&nome=$nome&distribuidora=$distribuidora&email=$email&telefone=$telefone&id_usuario=$id_usuario' class='btn btn-primary btn-sm' style='width:100px';>Editar</a> | 
            <a href='excluirfornecedor.php?id_fornecedor=$id' class='btn btn-primary btn-sm' style='width:100px';>Excluir</a>	
            </td></tr>";
}
echo "</table>";
// fechar a conexão
mysqli_close($link);

require '../padrao/rodape.php';