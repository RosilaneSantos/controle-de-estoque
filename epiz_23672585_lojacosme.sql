CREATE DATABASE lojacosme;

USE lojacosme;

CREATE TABLE IF NOT EXISTS usuario (
    id_usuario int(8) NOT NULL AUTO_INCREMENT UNIQUE,
    razao_social varchar(25), /* dos dois */
    cnpj char(14) NOT NULL, /* dos dois */
    email varchar(45) NOT NULL UNIQUE, /* dos dois */
    senha varchar(67) NOT NULL,  /* dos dois */
    endereco varchar(100), /* dos dois */
    telefone char(11), /* dos dois */
    responsavel varchar(45), /* empresa */
    nome varchar(45), /* funcionario */
    cpf char(11) NOT NULL, /* funcionario */
    sexo char(1), /* funcionario */
    tipo varchar(11), /* dos dois */
    PRIMARY KEY (id_usuario)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS regiao (
    id_rr int(8) NOT NULL AUTO_INCREMENT UNIQUE,
    uf char(2) NOT NULL,
    descricao varchar(20) NOT NULL,
	PRIMARY KEY (id_rr)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS cliente (
    id_cliente int(8) NOT NULL AUTO_INCREMENT UNIQUE,
    nome varchar(45) NOT NULL,
    cpf char(11) NOT NULL UNIQUE,
    email varchar(45) NOT NULL,
    dt_nasc date NOT NULL,
    telefone char(11),
    sexo char(1),
    id_rr int(8) NOT NULL,
    id_usuario int(8) NOT NULL,
    PRIMARY KEY (id_cliente),
    FOREIGN KEY (id_rr) REFERENCES regiao(id_rr),
    FOREIGN KEY (id_usuario) REFERENCES usuario(id_usuario)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS fornecedor (
    id_fornecedor int(8) NOT NULL AUTO_INCREMENT UNIQUE,
    nome varchar(45) NOT NULL,
    distribuidora varchar(45) NOT NULL,
    email varchar(45) NOT NULL,
    telefone char(11),
    id_usuario int(8) NOT NULL,
    PRIMARY KEY (id_fornecedor),
    FOREIGN KEY (id_usuario) REFERENCES usuario(id_usuario)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS produto (
    id_produto int(8) NOT NULL AUTO_INCREMENT UNIQUE,
    cod char(13) NOT NULL UNIQUE,
    nome varchar(45) NOT NULL,
    categoria varchar(45) NOT NULL,
    tamanho smallint(4) NOT NULL,
    marca varchar(45) NOT NULL,
    decricao varchar(200),
    valor decimal(6,2) NOT NULL,
    qt_estoque int(5) NOT NULL,
    id_usuario int(8) NOT NULL,
    id_fornecedor int(8) NOT NULL,
    PRIMARY KEY (id_produto),
    FOREIGN KEY (id_usuario) REFERENCES usuario(id_usuario),
    FOREIGN KEY (id_fornecedor) REFERENCES fornecedor(id_fornecedor)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO regiao
    (uf, descricao)
	VALUES
	  ('AC', 'Acre'),
    ('AL', 'Alagoas'),
    ('AM', 'Amazonas'),
    ('AP', 'Amapá'),
    ('BA', 'Bahia'),
    ('CE', 'Ceará'),
    ('DF', 'Distrito Federal'),
    ('ES', 'Espírito Santo'),
    ('GO', 'Goiás'),
    ('MA', 'Maranhão'),
    ('MT', 'Mato Grosso'),
    ('MS', 'Mato Grosso do Sul'),
    ('MG', 'Minas Gerais'),
    ('PA', 'Pará'),
    ('PB', 'Paraíba'),
    ('PR', 'Paraná'),
    ('PE', 'Pernambuco'),
    ('PI', 'Piauí'),
    ('RJ', 'Rio de Janeiro'),
    ('RN', 'Rio Grande do Norte'),
    ('RS', 'Rio Grande do Sul'),
    ('RO', 'Rondônia'),
    ('RR', 'Roraima'),
    ('SC', 'Santa Catarina'),
    ('SP', 'São Paulo'),
    ('SE', 'Sergipe'),
    ('TO', 'Tocantins');
