<!-- Alertas -->
  <div align="center">
    <!-- Login -->
    <?php if (isset($_GET["login"]) && $_GET["login"]==0) { ?>
        <br>
        <p class="alert-danger">ERRO nenhum usuario com esses dados</p>
    <?php } ?>
    <?php if (isset($_GET["loginS"]) && $_GET["loginS"]==0) { ?>
        <br>
        <p class="alert-danger">Senha ERRADA</p>
    <?php } ?>

    <!-- Cadastro -->
    <?php if (isset($_GET["u_cadastro"]) && $_GET["u_cadastro"]==0) { ?>
        <br>
        <p class="alert-success">Cadastrado com SUCESSO!</p>
    <?php } ?>
    <?php if (isset($_GET["u_cadastro"]) && $_GET["u_cadastro"]==1) { ?>
        <br>
        <p class="alert-danger">Não houve cadastro tente novamente</p>
    <?php } ?>
    <?php if (isset($_GET["u_cadastroF"]) && $_GET["u_cadastroF"]==0) { ?>
        <br>
        <p class="alert-success">Funcionário cadastrado com SUCESSO!</p>
    <?php } ?>
    <?php if (isset($_GET["u_cadastroF"]) && $_GET["u_cadastroF"]==1) { ?>
        <br>
        <p class="alert-danger">Não houve cadastro tente novamente</p>
    <?php } ?>

    <!-- Senha cadastro -->
    <?php if (isset($_GET["u_erroS"]) && $_GET["u_erroS"]==0) { ?>
        <br>
        <p class="alert-danger">ERRO! As senhas prescisão ser iguais</p>
    <?php } ?>

    <!-- Alterarção -->
    <?php if (isset($_GET["u_alt"]) && $_GET["u_alt"]==0) { ?>
        <br>
        <p class="alert-success">Dados alterados com SUCESSO!</p>
    <?php } ?>
    <?php if (isset($_GET["u_alt"]) && $_GET["u_alt"]==1) { ?>
        <br>
        <p class="alert-danger">ERRO não conseguimos alterar os seus dados</p>
    <?php } ?>

    <!-- Alterarção Senha-->
    <?php if (isset($_GET["u_altSN"]) && $_GET["u_altSN"]==0) { ?>
        <br>
        <p class="alert-success">Senha atualizada com SUCESSO!</p>
    <?php } ?>
    <?php if (isset($_GET["u_altSNC"]) && $_GET["u_altSNC"]==0) { ?>
        <br>
        <p class="alert-danger">Novas senhas digitadas não conferem.</p>
    <?php } ?>
    <?php if (isset($_GET["u_altAnC"]) && $_GET["u_altAnC"]==0) { ?>
        <br>
        <p class="alert-danger">Senha atual não confere.</p>
    <?php } ?>

    <!-- Exluir-->
    <?php if (isset($_GET["u_excl"]) && $_GET["u_excl"]==0) { ?>
        <br>
        <p class="alert-success">Conta excluida com SUCESSO!</p>
    <?php } ?>
    <?php if (isset($_GET["u_excl"]) && $_GET["u_excl"]==1) { ?>
        <br>
        <p class="alert-danger">ERRO não conseguimos excluir a sua conta</p>
    <?php } ?>
  </div>
