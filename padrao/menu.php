
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <a class="navbar-brand" href="http://localhost/telas_pi/usuario/">Home</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item">
        <a class="nav-link" href="#">Entrada de produto</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Saída de produto</a>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="" id="navbarDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Cadastro
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="http://localhost/telas_pi/cliente/index.php">Cadastrar cliente</a>
          <a class="dropdown-item" href="http://localhost/telas_pi/produto/index.php">Cadastrar produto</a>
          <a class="dropdown-item" href="http://localhost/telas_pi/fornecedor/index.php">Cadastrar fornecedor</a>
          <a class="dropdown-item" href="http://localhost/telas_pi/usuario/cadastroFun.php">Cadastrar funcionário</a>
        </div>
		</li>
		<li>
		<a class="nav-link dropdown-toggle" href="" id="navbarDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Listagens
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown" style="position:absolut;left:35%;">
          <a class="dropdown-item" href="http://localhost/telas_pi/cliente/listacliente.php">Listar cliente</a>
          <a class="dropdown-item" href="http://localhost/telas_pi/produto/listaproduto.php">Listar produto</a>
          <a class="dropdown-item" href="http://localhost/telas_pi/fornecedor/listafornecedor.php">Listar fornecedor</a>
          <a class="dropdown-item" href="http://localhost/telas_pi/usuario/listafuncionario.php">Listar funcionário</a>
        </div>
      </li>
  
    </ul>
    <button class="btn btn-outline-info my-2 my-sm-0" type="submit">
      <a href="../login/logout.php">Sair</a>
    </button>
  </div>
</nav>
